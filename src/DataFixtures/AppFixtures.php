<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Faker\Factory;
use Faker\Generator;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }
    

    public function load(ObjectManager $manager): void
    {
        for($i = 1; $i <= 5; $i++) {
            $category = new Category();
            $category->setName($this->faker->word())
                    ->setDescription($this->faker->words(3, true));
                
                $manager->persist($category);
        };
        
        $manager->flush();
    }
}