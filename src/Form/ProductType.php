<?php

namespace App\Form;

use App\Entity\Product;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('name', TextType::class, [
            'attr' => [
                'class' => 'form-control',
                'minlenth' => '2',
                'maxlength' => '100'
            ],
            'label' => 'Nom',
            'label_attr' => [
                'class' => 'form-label mt-4'
            ],
            'constraints' => [
                new Assert\Length(['min' => 2, 'max' => 100]),
                new Assert\NotBlank()
            ]
        ])
        ->add('description', TextareaType::class, [
            'attr' => [
                'class' => 'form-control',
                'minlenth' => '5',
                'maxlength' => '100'
            ],
            'label' => 'Description',
            'label_attr' => [
                'class' => 'form-label mt-4'
            ],
            'constraints' => [
                new Assert\Length(['min' => 5, 'max' => 100]),
                new Assert\NotBlank()
            ]
        ])
        ->add('quantity', IntegerType::class, [
            'attr' => [
                'class' => 'form-control',
                'min' => '1',
                'max' => '10000'
            ],
            'label' => 'Quantité',
            'label_attr' => [
                'class' => 'form-label mt-4'
            ],
            'constraints' => [
                new Assert\Length(['min' => 1, 'max' => 10000]),
                new Assert\NotBlank(),
                new Assert\Positive()
            ]
        ])
        ->add('price', MoneyType::class, [
            'attr' => [
                'class' => 'form-control',
            ],
            'label' => 'Prix/',
            'label_attr' => [
                'class' => 'form-label mt-4'
            ],
            'constraints' => [
                new Assert\Positive()
            ]
        ])
        ->add('category', EntityType::class, [
            'label' => 'Catégorie',
            'label_attr' => [
                'class' => 'form-label mt-4'
            ],
            'class' => Category::class,
            'choice_label' => 'name',
            'attr' => [
                'class' => 'form-control mb-4',
                'required' => false,
            ]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}