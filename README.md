Inscrivez-vous.
Les produits sont visibles dans la partie "liste des produits".
Une fois connecté, accédez à la partie "gestion des produits".
Créer, éditer, voir, supprimer des produits.
Dans la partie "gestion des produits", cliquez sur "Catégories".
Créer, éditer, voir, supprimer des catégories.
Possibilité de déconnexion.
Les pages /products et /catégories ne sont accessibles que si l'utilisateur est connecté.
