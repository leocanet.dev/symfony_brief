<?php

namespace ContainerPkGbBPx;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder5930a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2aa19 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties72589 = [
        
    ];

    public function getConnection()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getConnection', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getMetadataFactory', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getExpressionBuilder', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'beginTransaction', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getCache', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getCache();
    }

    public function transactional($func)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'transactional', array('func' => $func), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'wrapInTransaction', array('func' => $func), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'commit', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->commit();
    }

    public function rollback()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'rollback', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getClassMetadata', array('className' => $className), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'createQuery', array('dql' => $dql), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'createNamedQuery', array('name' => $name), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'createQueryBuilder', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'flush', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'clear', array('entityName' => $entityName), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->clear($entityName);
    }

    public function close()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'close', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->close();
    }

    public function persist($entity)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'persist', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'remove', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'refresh', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'detach', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'merge', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getRepository', array('entityName' => $entityName), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'contains', array('entity' => $entity), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getEventManager', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getConfiguration', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'isOpen', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getUnitOfWork', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getProxyFactory', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'initializeObject', array('obj' => $obj), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'getFilters', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'isFiltersStateClean', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'hasFilters', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return $this->valueHolder5930a->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer2aa19 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder5930a) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder5930a = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder5930a->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__get', ['name' => $name], $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        if (isset(self::$publicProperties72589[$name])) {
            return $this->valueHolder5930a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__isset', array('name' => $name), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__unset', array('name' => $name), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__clone', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $this->valueHolder5930a = clone $this->valueHolder5930a;
    }

    public function __sleep()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__sleep', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return array('valueHolder5930a');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer2aa19 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer2aa19;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'initializeProxy', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder5930a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5930a;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
