<?php

namespace ContainerOcLHNWr;
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-components/src/Knp/Component/Pager/PaginatorInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-components/src/Knp/Component/Pager/Paginator.php';

class PaginatorInterface_82dac15 implements \ProxyManager\Proxy\VirtualProxyInterface, \Knp\Component\Pager\PaginatorInterface
{
    /**
     * @var \Knp\Component\Pager\PaginatorInterface|null wrapped object, if the proxy is initialized
     */
    private $valueHolder5930a = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2aa19 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties72589 = [
        
    ];

    public function paginate($target, int $page = 1, ?int $limit = null, array $options = []) : \Knp\Component\Pager\Pagination\PaginationInterface
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'paginate', array('target' => $target, 'page' => $page, 'limit' => $limit, 'options' => $options), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        if ($this->valueHolder5930a === $returnValue = $this->valueHolder5930a->paginate($target, $page, $limit, $options)) {
            return $this;
        }

        return $returnValue;
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        $instance->initializer2aa19 = $initializer;

        return $instance;
    }

    public function __construct()
    {
        static $reflection;

        if (! $this->valueHolder5930a) {
            $reflection = $reflection ?? new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');
            $this->valueHolder5930a = $reflection->newInstanceWithoutConstructor();
        }
    }

    public function & __get($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__get', ['name' => $name], $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        if (isset(self::$publicProperties72589[$name])) {
            return $this->valueHolder5930a->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__isset', array('name' => $name), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__unset', array('name' => $name), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $realInstanceReflection = new \ReflectionClass('Knp\\Component\\Pager\\PaginatorInterface');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5930a;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder5930a;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__clone', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        $this->valueHolder5930a = clone $this->valueHolder5930a;
    }

    public function __sleep()
    {
        $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, '__sleep', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;

        return array('valueHolder5930a');
    }

    public function __wakeup()
    {
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer2aa19 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer2aa19;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2aa19 && ($this->initializer2aa19->__invoke($valueHolder5930a, $this, 'initializeProxy', array(), $this->initializer2aa19) || 1) && $this->valueHolder5930a = $valueHolder5930a;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder5930a;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5930a;
    }
}

if (!\class_exists('PaginatorInterface_82dac15', false)) {
    \class_alias(__NAMESPACE__.'\\PaginatorInterface_82dac15', 'PaginatorInterface_82dac15', false);
}
